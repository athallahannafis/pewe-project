from django.shortcuts import render
from django.http import HttpResponseRedirect

from .forms import PostForm
from .models import Post

# Create your views here.

name = "Nafis"
phone_number = "0813 4032 4415"
email = "athallahannafis@gmail.com"
line_ID = "annafis99"

# form
intro = "Welcome to my form!"
msg = "Please register yourself in this form, for us to know each other"

# gaming
introGaming = "I also play some video games"
msgGaming = "These are the games I play nowadays"

def index(request):
    response = {"mhs_name" : name}
    return render(request, "index.html", response)

def about(request):
    response = {"a" : name}
    return render(request, "story3-about.html", response)

def contact(request):
    response = {"number" : phone_number, "e" : email, "line" : line_ID}
    return render(request, "story3-contact.html", response)

def gaming(request):
    response = {"introGaming" : introGaming, "msgGaming" : msgGaming}
    return render(request, "story3-gaming(2).html", response)

def form(request):
    response = {"intro" : intro, "msg" : msg}
    return render(request, "story3-form.html", response)

def data_form(request):
    form = PostForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            form.save()
    events = Post.objects.all()
    return render(request, "act-form.html", {'form' : form, "events" : events})

def delete_all(request):
    print(request.method)
    if request.method == "POST":
        Post.objects.all().delete()
        print(Post.objects.all())
    return HttpResponseRedirect('/form/')