from django.db import models
from django.utils import timezone

# Create your models here.

class Post(models.Model):
    name = models.CharField(max_length=40)
    date = models.DateField()
    time = models.TimeField()
    location = models.CharField(max_length=20)
    
    # choice section
    QUIZ = 'Quiz'
    TASK = 'Task'
    MEETING = 'Meeting'
    SPORT = 'Sport'
    CATEGORY = (
        (QUIZ, 'Quiz'),
        (TASK, 'Task'),
        (MEETING, 'Meeting'),
        (SPORT, 'Sport'),
    )

    act_category = models.CharField(
        max_length=10,
        choices=CATEGORY,
        default="Select Category",
    )