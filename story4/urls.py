from django.conf.urls import url
from .views import index, about, contact, data_form, gaming, delete_all
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^about/$', about, name="about"),
    url(r"^contact/$", contact, name="contact"),
    url(r"^form/$", data_form, name="form"),
    url(r"^gaming/$", gaming, name="gaming"),
    url(r"^form/del_all/$", delete_all, name="del_form"),
    
    # "$" adalah end of string

]